// const  request = require("request");
const {
    servicios_web
} = require("./config_microservicio");
const micro = require("./servidor_micro");
const {
    puerto
} = require("./config_microservicio");


// const CONFIG_MICROSERVICIO = "./config_microservicio.json";
const subscribir_microservicio = require("./subscribir_microservicio");
global.configuraciones = require("./configuraciones");


console.log(" ███╗   ███╗██╗ ██████╗██████╗  ██████╗     ██╗      ██████╗  ██████╗ ██╗███╗   ██╗ ");
console.log(" ████╗ ████║██║██╔════╝██╔══██╗██╔═══██╗    ██║     ██╔═══██╗██╔════╝ ██║████╗  ██║ ");
console.log(" ██╔████╔██║██║██║     ██████╔╝██║   ██║    ██║     ██║   ██║██║  ███╗██║██╔██╗ ██║ ");
console.log(" ██║╚██╔╝██║██║██║     ██╔══██╗██║   ██║    ██║     ██║   ██║██║   ██║██║██║╚██╗██║ ");
console.log(" ██║ ╚═╝ ██║██║╚██████╗██║  ██║╚██████╔╝    ███████╗╚██████╔╝╚██████╔╝██║██║ ╚████║ ");
console.log(" ╚═╝     ╚═╝╚═╝ ╚═════╝╚═╝  ╚═╝ ╚═════╝     ╚══════╝ ╚═════╝  ╚═════╝ ╚═╝╚═╝  ╚═══╝ ");


servicios_web.forEach(({
    evento
}) => {
    if (evento.split(".", 3).length > 1) {
        var script = require(`./controladores/${ evento.split(".", 3)[1]}`);
    }
    micro.eventos[evento] = script;
});

// modulos_mongo().then(modulos=>{
//     global.modulos = modulos;
    micro.arrancar(puerto,function(puerto){
        subscribir_microservicio(puerto).then(respuesta => {
            console.log("respuesta subscripción: ", respuesta);
        }).catch(err=>{
            console.error("error al intentar subscribir el micro servicio: ", err);
        });
    });
// });
