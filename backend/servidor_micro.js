const micro = require('micro');
const {
    send,
    json,
    text
} = micro;

const server = micro(async (req, res) => {
    try {

        //console.log("llegando peticion");
        if (req.method !== "POST") {
            return send(res, 404, {
                "404": "El servicio no existe"
            });
        }
        //console.log("req.url: ",req.url);
        const evento = buscar_evento(req.url);
        const funcion = eventos[evento];
        if (!funcion) {
            return send(res, 404, {
                "404": "El servicio no existe"
            });
        }

        const texto_peticion = await text(req).catch(err => {
            throw err;
        });
        if (!texto_peticion) {
            return send(res, 400, {
                "400": "No se enviío ningun dato"
            });
        }
        if (!isJson(texto_peticion)) {
            return send(res, 400, {
                "400": "Se requiere objeto JSON"
            });
        }

        const js = await json(req).catch(err => {
            throw err;
        });

        const peticion = js.body || js;

        if (peticion.existe) {
            return send(res, 200, {
                existe: true
            })
        }

        const respuesta = await funcion(peticion).catch(err => {
            throw err;
        });

        send(res, 200, respuesta);
    } catch (err) {
        console.error("err: ", err);
        let codigo_estado = err.codigo_status ? err.codigo_status : 500;
        return send(res, codigo_estado, {
            "error": err.message
        });
    }

});

function buscar_evento(url) {
    const array = url.split("/");
    return (array.length > 1) ? array[1] : "";
}

function arrancar(puerto, cb) {
    try {
        if (!puerto) {
            puerto = PUERTO;
        }
        server.listen(puerto);
        const prueba = function(e) {
            console.log("escuchando micro en el puerto: " + puerto);
            if (cb && cb instanceof Function) {
                cb(puerto);
            }
        };
        server.removeAllListeners('listening');
        server.removeAllListeners('error');
        server.once('listening', prueba);
        server.on('error', function(e) {
            server.close();
            puerto++;
            arrancar(puerto, cb);
        });
    } catch (err) {
        console.log("err: ", err);
    }
}

const eventos = {

}

module.exports = {
    eventos,
    arrancar
}

function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
