//Declaramos dependencias
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var C = include("controladores/general/constantes");

var usuario_schema = new Schema({
    usuario: {
        type: String,
        required: true,
        minlength: [5, "El usuario debe tener minimo 5 caracteres"],
        lowercase: true,
        unique: true
    },
    contrasenia: {
        type: String
    },
    contrasenias_pasadas: [String],
    reportes: [{
        type: Number
    }],
    modulos: [{
        type: Number
    }],
    itrack_depositos: [{
        type: Number
    }],
    nombre: String,
    correo: {
        type: String,
        match: [/([\w+\._%-]+@[\w+\.-]+\.[\w+]{2,4}[^,;\s])/, "Correo inválido"]
    },
    base: {
        type: String,
        minlength: [5, "El nombre de la base debe ser de longitud 5"],
        maxlength: [5, "El nombre de la base debe ser de longitud 5"],
        uppercase: false
    },
    tipo: {
        type: String,
        default: 'usuario',
        enum: {
            values: ['usuario', 'administrador', 'desarrollo'],
            message: "No existe el tipo de usuario"
        }
    },
    activo: {
        type: Boolean,
        default: true
    },
    ws_activo: {
        type: Boolean,
        default: false
    },
    gis: {
        activo: {
            type: Boolean,
            default: false,
        },
        modulos: []
    },
    contrasenia_nueva: {
        type: Boolean,
        default: true
    },
    puede_cambiar_contrasenia: {
        type: Boolean,
        default: true
    },
    tiempo_sesion: {
        type: Number,
        default: 5
    },
    fecha_ultimo_login: {
        type: Date
    },
    perfil_usuario: {
        type: String
    },
    tipo_impresion: {
        type: String,
        default: "pdf"
    },
    bases: [],
    tlv_rol: {
        type: String,
        default: C.TELEVENTA_ROL_OPERADOR,
        enum: {
            values: [C.TELEVENTA_ROL_GERENTE, C.TELEVENTA_ROL_SUPERVISOR, C.TELEVENTA_ROL_OPERADOR,
                C.TELEVENTA_ROL_ADMINISTRADOR, C.TELEVENTA_ROL_DESARROLLO],
            message: "No existe el rol de Televenta"
        }
    }
});

/*usuario_schema.path('modulos').validate(async function(modulos_id) {
    var Modulo = require("../modulos");
    var modulos = await Modulo.count({
        _id: {
            $in: modulos_id
        }
    }).exec();
    if (modulos_id.length > 0 && modulos < 1) return false
    else return true;
}, "No existe el modulo");
*/

/*usuario_schema.path('usuario').validate(async function(usuario) {
    var usuario = await this.model('Usuario').findOne({
        usuario: usuario,
        _id: {
            $ne: this._id
        }
    }).lean().exec();
    if (usuario) return false;
    else return true;
}, "El usuario ya existe");*/

//antes de guardar usuario nuevo
usuario_schema.pre("save", function (next) {
    if (this.contrasenias_pasadas.indexOf(this.contrasenia) > -1) {
        next(new Error(C.MENSAJE_ERROR_CONTRASENIA_YA_USADA));
        return;
    }
    next();
});

//antes de actualizar usuario 
/*usuario_schema.pre("findOneAndUpdate", async function(next) {
    var actualizar = this._update.$set;

    if (actualizar.contrasenia != undefined) {
        var usuario = await this.findOne({}).lean().exec();
        if (usuario.contrasenias_pasadas.indexOf(actualizar.contrasenia) > -1) {
            next(new Error(C.MENSAJE_ERROR_CONTRASENIA_YA_USADA));
            return;
        }
    }

    next();
});
*/
//despues de actualizar usuario 
/*usuario_schema.post("findOneAndUpdate", async function(doc) {
    var actualizado = this._update.$set;
    if (actualizado.contrasenia != undefined) {
        var usuario = await this.findOne({}).lean().exec();
        var contrasenias_pasadas = usuario.contrasenias_pasadas;
        if (contrasenias_pasadas.length > C.CANTIDAD_CONTRASENIAS_PASADAS) {
            contrasenias_pasadas.shift(); //elimina el primer elemento
        }
        contrasenias_pasadas.push(actualizado.contrasenia);
        this.model.update({}, {
            contrasenias_pasadas: contrasenias_pasadas
        }).exec();
    }
    //cerrar sesion si estaba activa
    if (!doc.activo) {
        require("../../controladores/pwst_usuarios").cierra_sesion_usuario(doc);
    } else {
        //si al actualizar se modificaron modulos -> intentar recargar en tiempo real modulos de usuario
        if (actualizado.modulos)
            require("../../controladores/pwst_usuarios").notifica_cambio_de_modulos_a_usuario(doc);
    }

});*/

//Despues de guardar usuario nuevo
//Agrega el historial de contraseñas
/*usuario_schema.post('save', function(doc) {
    if (!(doc.tipo == C.TIPO_USUARIO || doc.tipo == C.TIPO_ADMINISTRADOR)) {
        var contrasenias_pasadas = doc.contrasenias_pasadas;
        if (contrasenias_pasadas.length > C.CANTIDAD_CONTRASENIAS_PASADAS) {
            contrasenias_pasadas.shift(); //elimina el primer elemento
        }
        contrasenias_pasadas.push(doc.contrasenia);
        this.model('Usuario').update({
                _id: doc._id
            }, {
                $set: {
                    contrasenias_pasadas: contrasenias_pasadas
                }
            },
            function(err, docs) {
                if (err) console.log(err);
            });
        require("../../controladores/" + C.MODULO_PWST_USUARIOS).enviar_notificacion_nuevo_usuario(doc);
    }
});

usuario_schema.post("findOneAndRemove", function(doc) {
    const Logger = require("../../controladores/logger");
    const usr = doc.usuario;

    console.log("doc eliminar: ",doc);

    require("../../controladores/" + C.MODULO_PWST_USUARIOS).cierra_sesion_usuario(usr);


    Logger.log_usuario(doc.base, usuario, C.PWST_USUARIOS, "Se ha eliminado a el usuario " + usuario_a_borrar);
});
*/
module.exports = usuario_schema;