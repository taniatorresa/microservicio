const C = include("controladores/general/constantes");
var md5 = require('md5');

//Declaramos dependencias
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const tipos_api = ['eal', 'and', 'dll', 'rst', 'ped'];

var conexion_base_configuraciones_schema = new Schema({
    usuario: {
        type: String,
        required: [true, 'Usuario requerido']
    },
    clave: {
        type: String,
        required: [true, 'Contraseña requerida']
    },
    //Se elimino propiedad de servidor'localhost\\SQLEXPRESS'
    base: {
        type: String,
        minlength: [5, 'La base de datos debe contener 5 caracteres'],
        maxlength: [5, 'La base de datos debe contener 5 caracteres'],
        required: [true, 'Base de datos requerida'],
        index: {
            unique: true
        }
    },
    zona_horaria: {
        id: {
            type: Number,
            default: 0
        },
        diferencia: {
            type: Number,
            default: 0
        }
    },
    modulos: [{
        type: Number
    }],
    horario_verano: Boolean,
    pais: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Pais',
        CastError: "No se encontro el pais seleccionado"
    },
    subdominio_pedidos: String,
    base_independiente: {
        type: Boolean,
        default: false
    },
    lat: Number,
    lng: Number,
    eliminacion_pendientes: Date,
    api_key: [{
        key: {
            type: String,
            unique: true,
            sparse: true
        },
        tipo: {
            type: String,
            enum: tipos_api
        }
    }],
    base_central: {
        type: String,
        default: ""
    },
    url_web_services: {
        type: String,
        default: ""
    },
    url_login_services: {
        type: String,
        default: ""
    },
    comentario: {
        type: String,
        default: ""
    },
    conexion_con_usuario: {
        type: Boolean,
        default: false
    },
}, {
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true
    },
    collection: 'conexion_base_configuraciones'
});


conexion_base_configuraciones_schema.virtual("pais_datos")
    .get(function () {
        return this.p_datos;
    })
    .set(function (datos) {
        this.p_datos = datos;
    });

conexion_base_configuraciones_schema.virtual("conectar")
    .get(function () {
        return this.conexion;
    }).set(function (config) {
        this.conexion = config;
    });

conexion_base_configuraciones_schema.virtual("prefijo")
    .get(function (config) {
        return this.conexion;
    }).set(function (config) {
        this.conexion = config;
    });


conexion_base_configuraciones_schema.pre("init", async function (next, datos) {
    const Pais = require("./paises");
    var paises = await Pais.findOne({
            _id: datos.pais
        }).lean().exec()
        .catch(err => {
            console.error("conexion_base_configuraciones, no se pudieron obtener los  datos del pais: ", err);
        });
    this.pais_datos = paises;
    next();
});


//Conexion valida
conexion_base_configuraciones_schema.pre("save", function (next) {
    this.api_key = generar_apis(this.base);

    function validar_conexion() {
        return true;
    }
    if (!validar_conexion()) {
        next(new Error("Conexion no valida"));
        return;
    }
    next();
});

conexion_base_configuraciones_schema.pre("findOneAndUpdate", async function (next) {
    var datos = await this.model.findOne(this._conditions).lean().exec().catch(err => {
        console.error(err);
    });

    const actualizar = this._update.$set;
    if (!datos.api_key || datos.api_key) {
        actualizar.api_key = generar_apis(actualizar.base);
    }

    if (actualizar.pais != undefined && actualizar.pais == "") {
        next(new Error("Se debe seleccionar un pais"));
        return;
    }

    const Usuarios = await include("controladores/general/conexiones_bases_clientes")(C.MODELOS_CLIENTES_USUARIOS, actualizar.base)
        .catch(err => {
            console.error(err);
            return;
        });


    /*Siguiente código permite relacionar los modulos seleccionados en la base con los modulos mostrados al usuario, actualizando modulos del usuarios para evitar recuperar los datos con valor false*/
    var lista_usuarios = await Usuarios.find({}).lean().exec();
    var c = [];

    if (actualizar.modulos) {
        for (let i_usuarios = 0; i_usuarios < lista_usuarios.length; i_usuarios++) {
            var usuario = lista_usuarios[i_usuarios];
            for (let j_modulos_base = 0; j_modulos_base < actualizar.modulos.length; j_modulos_base++) {
                var modulo_actual = actualizar.modulos[j_modulos_base];
                for (let k_modulos_usuario = 0; k_modulos_usuario < usuario.modulos.length; k_modulos_usuario++) {
                    if (usuario.modulos[k_modulos_usuario] == modulo_actual) {
                        c = c.concat(modulo_actual);
                        break;
                    }
                }
            }
            await Usuarios.findOneAndUpdate({
                usuario: usuario.usuario
            }, {
                $set: {
                    modulos: c
                }
            }, {
                upsert: false
            });

            c = [];
        }
    }
    next();
});

generar_apis
conexion_base_configuraciones_schema.post("findOneAndRemove", function (doc) {
    /*  const id = doc._id;
     require(C.MODULO_PWST_REVISA_ESTADO_CONEXIONES).notifica_errores({
         id: id,
         ok: true,
         codigo: C.PWST_CONFIGURACIONES_BASES_NOTIFICACION_BASE_ELIMINADA
     }); */

});

function generar_apis(base) {
    return tipos_api.map(tipo => {
        return {
            key: md5(base + tipo),
            tipo: tipo
        };
    });
}

var Conexion_base_configuracion = mongoose.model("Conexion_base_configuracion", conexion_base_configuraciones_schema);

module.exports = Conexion_base_configuracion;