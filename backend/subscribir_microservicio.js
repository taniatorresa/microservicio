const request = require('request');
const microservicio = require("./config_microservicio");

module.exports = puerto => new Promise((resolver, rechazar) => {
    microservicio.puerto = puerto;
    request.post(configuraciones.url_subscripcion_microservicio, {
        json: microservicio
    }, (error, res, body) => {
        if (error) {
            return rechazar(error);
        }
        if (body.microservicio)
            return resolver(true);
        else
            rechazar(body);
    });
})
