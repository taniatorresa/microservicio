angular.module("login")
    .controller("fondo", function($scope) {
        const URL_S3_IMAGENES_LOGIN = "https://s3.us-east-2.amazonaws.com/pwstplataforma/imagenes_login";
        let calculo_imagen = Math.floor(Math.random() * 11) + 1;
        const IMAGEN = calculo_imagen < 10 ? "0" + calculo_imagen : calculo_imagen;
        const PROPIEDAD_URL_FONDO = "url('URL_S3_IMAGENES_LOGIN/IMAGEN.jpg' )"
            .replace("URL_S3_IMAGENES_LOGIN", URL_S3_IMAGENES_LOGIN)
            .replace("IMAGEN", IMAGEN);
        $scope.estilo = {
            "background-image": PROPIEDAD_URL_FONDO + ",radial-gradient(circle, #abc7f8, #8096c1, #57688c, #303e5b, #0c182e)"
        }
    });
