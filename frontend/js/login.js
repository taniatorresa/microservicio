angular.module("login")
    .controller("login", function($scope, web_services, $window, $cookies, $timeout) {
        $scope.parametros = {};
        $scope.errores = "";

        $timeout(function() {
            console.log("login cookie");
            login_con_cookie();
        });


        $scope.cerrar_sesion_remota = function() {
            console.log("cerrar_sesion_remota: ");
            $scope.parametros.sacar_usuario = true;
            $scope.acceder();
        }

        const login_con_cookie = function() {
            $scope.errores = "";
            $scope.en_sesion = false;
            const s = $cookies.get('s');
            console.log("s: ",s);
            web_services({
                ruta: "login/login_con_cookie",
                body: {cookie: s}
            }).then(function(respuesta) {
                console.log("respuesta: ",respuesta);
                if (respuesta.error && Array.isArray(respuesta.error)) {
                    //$scope.errores = respuesta.error.join(", ");
                } else if (respuesta.error) {
                    if (respuesta.error == "Usuario en sesion") {
                        //$scope.en_sesion = true;
                    }
                    //$scope.errores = respuesta.error;
                } else {
                    let expira = new Date();
                    expira.setTime(expira.getTime() + respuesta.tiempo_sesion * 1000 * 60);
                    $cookies.put("s", respuesta.cookie, {
                        'expires': expira,
                        'path': "/"
                    });
                    redireccionar();
                }
            }).catch(function(err) {
                console.log("err: ", err);
                $scope.errores = err.error.join(", ");
            }).finally(function() {
                $scope.$apply();
            });
        }

        $scope.acceder = function() {
            $scope.errores = "";
            $scope.en_sesion = false;
            web_services({
                ruta: "login/login",
                body: $scope.parametros
            }).then(function(respuesta) {
                console.log("respuesta: ", respuesta);
                if (respuesta.error && Array.isArray(respuesta.error)) {
                    $scope.errores = respuesta.error.join(", ");
                } else if (respuesta.error) {
                    if (respuesta.error == "Usuario en sesion") {
                        $scope.en_sesion = true;
                    }
                    $scope.errores = respuesta.error;
                } else {
                    let expira = new Date();
                    expira.setTime(expira.getTime() + respuesta.tiempo_sesion * 1000 * 60);
                    $cookies.put("s", respuesta.cookie, {
                        'expires': expira,
                        'path': "/"
                    });
                    redireccionar();
                }
            }).catch(function(err) {
                console.log("err: ", err);
                $scope.errores = err.error.join(", ");
            }).finally(function() {
                $scope.$apply();
            });
        }


        function redireccionar() {
            //Esto esta hardcodeado, hay que cambiarlo con la respuesta de los modulos
            $window.location.href = '/pos_lite';
        }

    });
