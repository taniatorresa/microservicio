angular.module("login")
    .factory("web_services", function($http, $location) {
        const url = $location.protocol() + "://" + $location.host() + "/api/";
        return function(parametros) {
            return new Promise(function(resolver, rechazar) {
                if (parametros.ruta)
                    parametros.url = url + parametros.ruta;
                parametros = Object.assign({
                    metodo: "POST",
                    headers: {
                        'Content-Type': "application/json"
                    }
                }, parametros);
                const req = {
                    method: parametros.metodo,
                    url: parametros.url,
                    headers: parametros.headers,
                    data: parametros.body
                }
                $http(req).then(function(respuesta) {
                    resolver(respuesta.data);
                }, function(respuesta) {
                    respuesta.data.req = req;
                    rechazar(respuesta.data);
                });
            })
        }
    });
