const handler = require('serve-handler');
const http = require('http');
const PUERTO = 3000;
const subscribir_frontend = require("./subscribir_frontend");

const server = http.createServer((req, res) => {
    if (urls_bloqueadas.test(req.url)) {
        res.statusCode = 404;
        return res.end("No existe el servicio");
    }
    return handler(req, res);
});

const urls_bloqueadas = /^\/index.js|^\/node_modules|^\/package-lock.json|^\/package.json|^\/parametros_frontend.json|^\/subscribir_frontend.js/i;

function arrancar(puerto, cb) {
    try {
        if (!puerto) {
            puerto = PUERTO;
        }
        server.listen(puerto);
        const prueba = function(e) {
            console.log("escuchando micro en el puerto: " + puerto);
            if (cb && cb instanceof Function) {
                cb(puerto);
            }
        };
        server.removeAllListeners('listening');
        server.once('listening', prueba);
        server.removeAllListeners('error');
        server.once('error', function(e) {
            server.close();
            puerto++;
            arrancar(puerto, cb);
        });
    } catch (err) {
        console.log("err: ", err);
    }
}

arrancar(PUERTO, function(puerto){
    subscribir_frontend(puerto).then(respuesta => {
        console.log("Respuesta de subscribción de front end: ", respuesta);
    }).catch(err => {
        console.error(err);
    });
});
