const request = require('request');
const {ruta, archivo_predeterminado, url_servidor} = require("./parametros_frontend");

const subscribir_frontend = puerto => new Promise((resolver, rechazar) => {
    request.post(url_servidor, {
        json: {
            puerto,
            ruta,
            archivo_predeterminado
        }
    }, (error, res, body) => {
        if (error) {
            return rechazar(error);
        }
        if (body && body.subscrito)
            return resolver(true);
        else
            rechazar(body);
    });
});
module.exports = subscribir_frontend;
